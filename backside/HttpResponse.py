import re
from backside import Constants

__author__ = 'Volodymyr Piskun'
import SETTINGS
from backside.Constants import STATUS_CODE, URL_BUILDER
from backside import Constants, url_namespace



class HttpResponse:
    """
    The class, that created to build http response to the client-side.
    """

    def __init__(self):
        self.code = STATUS_CODE.SUCCESS[200]
        self.httpver = 'HTTP/1.1'
        self.headers = {"Server": "BlackHoleBox / 0.0.0.1.1", "Content-Type": "text/html", "charset": "utf-8",
                        "Connection": "close"}
        self.content = ''

    def addHeader(self, name, content):
        """
        Adds or edits http-header
        :param name:
        :param content:
        :return:
        """
        if type(name) == str or type(content) == str:
            self.headers[name] = content
        else:
            raise Exception("Incorrect type of name/content of header")

    def rmHeader(self, name):
        """
        Removes http-header
        :param name:
        :return:
        """
        if type(name) == str:
            del self.headers[name]

    def check_location(self, HttpRequest):
        """
        Checks - is file exists, if exists return their content

        :param HttpRequest:
        :return:
        """
        if SETTINGS.SETTINGS.ULR_BUILDER_TYPE == URL_BUILDER.FOLDER_CONTENT:
            # If in settings set Folder content type
            data = url_namespace.url_namespace.build_way(HttpRequest.location)
            if data:
                print(data)
                print('Check location')
                return data
            else:
                return False






                # TODO: ADD ANOTHER constr.
                # else:
                #     pass

    def compile(self, HttpRequest):
        """
        compile the response, with headers and body
        :return: response to the browser
        """
        self.addHeader('Content-Type', 'text/html')
        if self.check_location(HttpRequest):
            # if file exists - return their content
            self.content = self.check_location(HttpRequest)
            self.code = STATUS_CODE.SUCCESS[200]
            fileextention = re.search('\..[^\.]+$', HttpRequest.location).group()[1:]
            if fileextention in Constants.EXTENTIONS.EXTENTIONS:
                # content-type switch
                for ext, tpe in zip([Constants.EXTENTIONS.images,
                                     Constants.EXTENTIONS.txt, Constants.EXTENTIONS.audio, Constants.EXTENTIONS.video],
                                    [['image/'], ['text/'], ['audio/'], ['video/']]):

                    if fileextention in ext:
                        if fileextention == 'js':
                            fileextention = 'javascript'

                        self.addHeader('Content-Type', tpe[0] + fileextention)
                        break






        else:
            self.code = STATUS_CODE.CLIENT_ERROR[404]
            # switch to 404 error, if file isnt exist

        if self.code in STATUS_CODE.SERVER_ERROR.values() or self.code in STATUS_CODE.CLIENT_ERROR.values():
            # if there is some error -> returns error template

            self.content = '<!DOCTYPE><html><head> <title>ERROR %s</title></head><body><div style="margin-top: 109px;align-content: center;text-align: center;font-weight: bold;"><p style="">%s</p></div><hr><div style="font-style: italic; color: rgb(44, 44, 44); text-align: center;">BlackHole server <div></div></div></body></html>' % (
                self.code, self.code)
            print('\nERROR CODE. Return ERROR-page\n')

        headers = "%s %s \r\n" % (self.httpver, self.code)
        headers += "Content-Type: %s; charset=%s \r\n" % (self.headers['Content-Type'], self.headers['charset'])
        # all http-headers in one variable
        for key, value in zip(self.headers.keys(), self.headers.values()):
            if key != "charset" and key != "Content-Type":
                headers += "%s: %s \r\n" % (key, value)

        result = headers + "\r\n\r\n" + self.content
        # response body(headers+page content
        return result
