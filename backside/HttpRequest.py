__author__ = 'Volodymyr Piskun'
import re
from pprint import pprint
import SETTINGS


#
# response_example = ['GET /index.html HTTP/1.1', 'Host: 127.0.0.1:2050',
#                     'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
#                     'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#                     'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', 'DNT: 1',
#                     'Connection: keep-alive', '', '']


class HttpRequest:
    """
    Http Request parser class
    """

    def __init__(self, response_string):
        # httpver = re.compile('[A-Z]+\s/').match(response_string[0]).end()
        # print response_string[0][]
        # httptype = re.compile('[A-Z]+').match(response_string[0]).end()
        headers = {
            # 'httpver': response_string[0][httpver:],
            #        'type': response_string[0][:httptype],
            # 'location': response_string[0][httptype:httpver]
        }

        response_string[0] = response_string[0].split(' ')
        print(response_string[0])
        headers = {
            'httpver': response_string[0][2],
            'type': response_string[0][0],
            'location': response_string[0][1]
        }
        for x in response_string[1:]:
            if len(x) > 0:
                parse_res = self.parse_header(x)
                headers[parse_res[0]] = parse_res[1]


        # VARs define
        self.host = headers['host']
        self.type = headers['type']
        self.httpver = headers['httpver']
        self.user_agent = headers['user-agent']
        self.accept = {'accept': headers['accept'], 'lang': headers['accept-language'],
                       'encoding': headers['accept-encoding']}
        self.connection = headers['connection']
        self.location = headers['location'].replace(' ', '')

        # TODO: Fuck this shit
        if SETTINGS.INDEX_PAGE is None:
            if self.location == '/':
                self.location = '/index.html'
            self.debug()
        else:
            self.location = SETTINGS.SETTINGS.INDEX_PAGE
            # self.host = self.parse_header(response_string[1])  # 127.0.0.1
            # self.user_agent = self.parse_header(response_string[2])  # Mozilla
            # self.accept = self.parse_header()

            #
            # for x in response_string[1:]:
            #     headers.append(self.parse_header(x))

    @staticmethod
    def parse_header(header):
        """
        Method for parse http-headers
        :param header:
        :return:
        """
        re_expr = re.compile('\A[a-zA-Z)]+:|\A[a-zA-Z)]+-[a-zA-Z)]+:|^.+:')
        re_match = re_expr.match(header)
        return header[:re_match.end() - 1:].lower(), header[re_match.end() + 1:]

    def debug(self):
        """
        Prints all values of object
        :return:
        """
        pprint(vars(self))

# HttpRequest(response_example)
