# coding=utf-8
import SETTINGS

__author__ = 'Volodymyr Piskun'
import socket
from backside import HttpRequest, HttpResponse

class HttpServer:
    def __init__(self, port, mxcon, rcvpcs, debug):
        self.port = port
        self.max_connections = mxcon
        self.rcvpcs = rcvpcs
        self.debug = debug
        self.on = True
        self.data = ['GET /index.html HTTP/1.1', 'Host: 127.0.0.1:2050',
                     'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0',
                     'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                     'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', 'DNT: 1',
                     'Connection: keep-alive', '', '']
        # data example, for first run...(there is some shitcode)

    # @staticmethod
    # def send(conn,data):
    #     conn.send(data)

    def start(self):
        """
        Runs server
        :return:
        """
        # if self.debug:
        #     counter = 0
        httpresponse = HttpResponse.HttpResponse()
        # creating httpresponse instance
        sock = socket.socket()  # init socket
        sock.bind(('', self.port))  # binding port
        while self.on:
            while self.on:

                # counter +=1
                sock.listen(1002)  # port listening on(max connections - 1)
                conn, addr = sock.accept()  # accept возвращает два кортежа, мы их присваиваем переменным
                data = conn.recv(2048)
                # recive data in n-bytes
                httprequest = HttpRequest.HttpRequest(data.decode().split('\r\n'))  # HttpRequest instance

                if SETTINGS.SETTINGS.DEBUG:
                    httprequest.debug()
                    # runs debug method - if debug is on

                if not data:
                    break
                if SETTINGS.SETTINGS.DEBUG:
                    print('\n---------DATA------')
                    print(data)
                    print('----------/DATA-----------')
                data = httpresponse.compile(httprequest)
                # complile the answer dody
                if SETTINGS.SETTINGS.DEBUG:
                    print("\n\n\n" + data + "\n\n\n")
                conn.send(data.encode())
                # if self.debug:
                #     print counter
                conn.close()

    def stop(self):
        self.on = False

    def raise_error(self, error, error_type='warn'):
        if error_type != 'warn':
            self.stop()

        raise Exception(error)
