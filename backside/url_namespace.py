# coding=utf-8
__author__ = 'Volodymyr Piskun'
import os

import SETTINGS


class url_namespace:
    try:
        if SETTINGS.SETTINGS.DEBUG:
            print("-------------------\nCreating directory 'www'\n-------------------")
        os.mkdir('www')
    except:
        if SETTINGS.SETTINGS.DEBUG:
            print("Directory with name 'www' already exists")
    os.chdir('www')
    print(os.getcwd())

    pathcontent = []
    for d, dirs, files in os.walk('.'):
        for f in files:
            path = os.path.join(r'' + d, r'' + f)[2:]  # формирование адреса
            pathcontent.append('/'.join(path.split('\\')))  # добавление адреса в список
    # www dir recursive content-list

    @staticmethod
    def build_way(location):
        location = location[1:]
        if SETTINGS.SETTINGS.DEBUG:
            print ('--------pathcontent------')
            print (url_namespace.pathcontent)
            print ('-------------------------')

        if location in url_namespace.pathcontent:
            # reads file
            f = open(location, 'r')
            info = f.read()

            print ('------bildway----------\n' + info + '\n buildway[namespace]\n-----------------end-----------')
            f.close()
            return info

        else:
            #if there no file in www folder - returns False
            return False

