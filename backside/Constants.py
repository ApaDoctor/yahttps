__author__ = 'Volodymyr Piskun'


class STATUS_CODE:
    """
    Status code constant class
    """
    INFORMATIONAL = {
        100: '100 Continue',
    }
    SUCCESS = {
        200: '200 OK'
    }
    REDIRECTION = {
        302: '302 FOUND'
    }
    CLIENT_ERROR = {
        400: '400 Bad Request',
        401: '401 Unauthorized',
        403: '403 Forbidden',
        404: '404 NOT FOUND',
        451: '451 Unavailable For Legal Reasons',

    }
    SERVER_ERROR = {
        500: '500 Internal Server Error',
        502: '502 Bad Gateway',
        504: '504 Gateway Timeout',
    }


class URL_BUILDER:
    """
    Settings value constant class
    """
    FOLDER_CONTENT = 'directory_content'


class EXTENTIONS:
    """
    Extentions constant class
    """
    images = ['png', 'jpg', 'jpeg']
    txt = ['html', 'txt', 'css', 'js']
    audio = ['mp3']
    video = ['mp4']

    EXTENTIONS = images + txt + audio + video
