# coding=utf-8
__author__ = 'Volodymyr Piskun'
import SETTINGS
from backside import HttpResponse, HttpServer, HttpRequest

server = HttpServer.HttpServer(SETTINGS.PORT, SETTINGS.MAX_CONNECTIONS, SETTINGS.RECV_DATA_SIZE, SETTINGS.DEBUG)
# http_response = HttpResponse.HttpResponse()
# http_response.content = """"""
# http_request = HttpRequest.HttpRequest(server.data)
# http_response.code = HttpResponse.STATUS_CODE.SUCCESS[200]
server.start()

