__author__ = 'Volodymyr Piskun'
from backside.Constants import URL_BUILDER
class SETTINGS:
    PORT = 80
    # What port server will use. Default value is 80

    MAX_CONNECTIONS = 100
    # Number of max connections in queue

    RECV_DATA_SIZE = 1024
    # Size of pieces of data

    DEBUG = True
    # Debug mode

    ULR_BUILDER_TYPE = URL_BUILDER.FOLDER_CONTENT
    # Type of URL_BUILDER. If used 'directory_content' -> sitemap will be builded from 'www'-content.

    INDEX_PAGE = None
    # for example: '/index.html' or '/folder/page.html'
